import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './shared/error-page/error-page.component';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then( m => m.AuthModule )
  },
  {
    path: 'maintenance/categories',
    loadChildren: () => import('./maintenance/categories/categories.module').then( m => m.CategoriesModule )
  },
  {
    path: 'maintenance/sub-categories',
    loadChildren: () => import('./maintenance/sub-categories/sub-categories.module').then( m => m.SubCategoriesModule )
  },
  {
    path: 'maintenance/products',
    loadChildren: () => import('./maintenance/products/products.module').then( m => m.ProductsModule )
  },
  {
    path: 'maintenance/customers',
    loadChildren: () => import('./maintenance/customers/customers.module').then( m => m.CustomersModule )
  },
  {
    path: '404',
    component: ErrorPageComponent
  },
  {
    path: '**',
    // component: ErrorPageComponent
    redirectTo: '404'
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
