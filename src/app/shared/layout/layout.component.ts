import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styles: [`
  .container {
    margin: 10px;
  }
  #sidenav{
    width:240px !important;
  }

  #sidenav.menu-close{
    width:70px !important;
  }
  #sidenav.menu-open{
    width:240px;
  }

  `
  ]
})
export class LayoutComponent implements OnInit {

  isMenuOpen:boolean = true;

  constructor() { }

  ngOnInit(): void {
  }
  onToolBarMenuToggle():void{
        this.isMenuOpen=!this.isMenuOpen;
  }
}
