import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubCategoriesRoutingModule } from './sub-categories-routing.module';
import { ListComponent } from './pages/list/list.component';
import { AddComponent } from './pages/add/add.component';
import { ViewComponent } from './pages/view/view.component';
import { HomeComponent } from './pages/home/home.component';


@NgModule({
  declarations: [
    ListComponent,
    AddComponent,
    ViewComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    SubCategoriesRoutingModule
  ]
})
export class SubCategoriesModule { }
