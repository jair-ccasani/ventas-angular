import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddComponent } from './pages/add/add.component';
import { HomeComponent } from './pages/home/home.component';
import { ListComponent } from './pages/list/list.component';
import { ViewComponent } from './pages/view/view.component';

const routes: Routes = [
  {
    path:'',
    component:HomeComponent,
    children:[
      {path:'list',component:ListComponent},
      {path:'add',component: AddComponent},
      {path:'view',component: ViewComponent},
      {path:'edit:id',component: AddComponent},
      {path:'**',redirectTo:'list'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
