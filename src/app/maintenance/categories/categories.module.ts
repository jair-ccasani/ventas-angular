import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { ListComponent } from './pages/list/list.component';
import { AddComponent } from './pages/add/add.component';
import { ViewComponent } from './pages/view/view.component';
import { HomeComponent } from './pages/home/home.component';
import { MaterialModule } from '../../material/material.module';


@NgModule({
  declarations: [
    ListComponent,
    AddComponent,
    ViewComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    MaterialModule
  ]
})
export class CategoriesModule { }
